package pl.aleksandria_Library.library;


import pl.aleksandria_Library.items.Item;
import java.util.ArrayList;
import java.util.List;


public  class AleksandriaLibrary {


    private  List<Item> itemsInLibrary = new ArrayList<>();


    public  List<Item> getItemsInLibrary() {
        return itemsInLibrary;
    }

    /**
     *
     * @param item Element który chcemy dodać do listy itemów
     * @return true jeśli element został dodany jako nowy lub został powiększony stan tej samej pozycji
     */
    public boolean addItem(Item item) {

        for (Item element : itemsInLibrary) {
            if (element.equals(item)) {
                element.setHowManyHave(element.getHowManyHave() + 1);
                return true;
            }
        }
        if (!itemsInLibrary.contains(item)) {
            return itemsInLibrary.add(item);
        }
        return false;
    }


    /**
     * @param title       tytuł usuwanej książki
     * @param author  author usuwanej książki
     * @param kindOfClass klasa biektu który usuwamy
     * @return true jeśli książka zostanie usunięta lub zmniejszony stan dajen pozycji
     */
    public boolean removeItem(String title, String author, Class kindOfClass) {
        for (Item element : itemsInLibrary) {

            if (element.getTitle().equals(title) && element.getAuthor().equals(author) && element.getClass().equals(kindOfClass)) {
                if (element.getHowManyHave() > 1) {
                    element.setHowManyHave(element.getHowManyHave() - 1);
                    return true;
                }
                return itemsInLibrary.remove(element);
            }
        }
        return false;
    }



    public void showAllItems() {
        if (itemsInLibrary.isEmpty()) {
            System.out.println("brak pozycji");
        }
        for (Item element : itemsInLibrary) {
            System.out.println(element.toString());
        }
    }

//todo interfejs do metod?!

    /**
     * @param title tytul szukanej pozycji
     * @return lista posiadajaca w tytule fraze wpisana w parametr
     */
    public List<Item> findByTittle(String title) {
        List<Item> list = new ArrayList<>();

        for (Item element : itemsInLibrary) {

            if (element.getTitle().contains(title)) {
                list.add(element);
                System.out.println(element.getTitle());
            }
        }
        if (list.isEmpty()) {
            System.out.println("brak pozycji");
        }
        return list;
    }


}
