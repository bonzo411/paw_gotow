package pl.aleksandria_Library.library;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import pl.aleksandria_Library.items.Audiobook;
import pl.aleksandria_Library.items.Book;
import pl.aleksandria_Library.users.Admin;
import pl.aleksandria_Library.users.Reader;


public class Aleksandria extends Application {
    private Stage window;
    private Scene sceneAdmin, sceneUser, sceneStartowa;



    public static void main(String[] args) {
        launch(args);
    }


    @Override
    public void start(Stage primaryStage) throws Exception {
        //tworzenie listy itemów
          AleksandriaLibrary listaItems = new AleksandriaLibrary();
          UsersManager listaUsers = new UsersManager();

          window = primaryStage;
          window.setTitle("Aleksandria");


          //dla admina pole
          Button buttonAdBook = new Button("Add Book");
          Button buttonAdAudioBook = new Button("Add AudioBook");
          Button buttonRemoveItem = new Button("Remove Item");
          Button buttonAdUser = new Button("Add User");
          Button buttonRemoveUser = new Button("Remove User");
          Button buttonComeBack = new Button("Back");
          buttonComeBack.setOnAction(e -> window.setScene(sceneStartowa));


          VBox menuAdmin = new VBox(10);
          menuAdmin.getChildren().addAll(buttonAdBook,buttonAdAudioBook,buttonRemoveItem,buttonAdUser,buttonRemoveUser, buttonComeBack);

          sceneAdmin = new Scene(menuAdmin, 150,300);


        //dla usera pole
        Button buttonFind = new Button("Find Item");
        Button buttonShow = new Button("Show All Item");
        Button buttonBack = new Button("Back");
        buttonBack.setOnAction(e -> window.setScene(sceneStartowa));

        HBox menuUser = new HBox(10);
        menuUser.getChildren().addAll(buttonFind,buttonShow, buttonBack);
        sceneUser = new Scene(menuUser, 400,150);


        ///// plansza startowa
          Button startowyAdmin = new Button("Admin");
          startowyAdmin.setOnAction(e -> window.setScene(sceneAdmin));

          Button startowyUser = new Button("User");
          startowyUser.setOnAction(e -> window.setScene(sceneUser));

          HBox layout1 = new HBox();
          layout1.getChildren().addAll(startowyAdmin,startowyUser);

          BorderPane menu = new BorderPane();
          menu.setCenter(layout1);
          sceneStartowa = new Scene(menu,300,300);


    /// do wpisywania dla admina
        //add person
        TextField p1 = new TextField("name");
        TextField p2 = new TextField("last name");
        TextField p3 = new TextField("email");
        TextField p4 = new TextField("phone number");
        TextField p5 = new TextField("pesel");

        Button add = new Button("Add Reader");
        add.setOnAction(e ->{
            boolean message = listaUsers.addPerson(new Reader(p1.getText(), p2.getText(),p3.getText(),p4.getText(),p5.getText()));
            UtilForJavaFX.showAlertMessage(String.valueOf(message));
            window.setScene(sceneAdmin);
        });

        Button add2 = new Button("Add Admin");
        add2.setOnAction(e -> {
           boolean message = listaUsers.addPerson(new Admin(p1.getText(), p2.getText(), p3.getText(), p4.getText(), p5.getText()));
            UtilForJavaFX.showAlertMessage(String.valueOf(message));
            window.setScene(sceneAdmin);
        });

        FlowPane root = new FlowPane();
        root.setPadding(new Insets(10));
        root.getChildren().addAll(p1, p2, p3,p4,p5,add,add2);
        Scene addPesron = new Scene(root, 200, 400);


        /////// remove person
        TextField peselToRemove = new TextField("wpisz pesel do skasowania");
        Button removeP = new Button("remove person");
        removeP.setOnAction(e -> {
               boolean message = listaUsers.removePerson(peselToRemove.getText());
               UtilForJavaFX.showAlertMessage(String.valueOf(message));
               window.setScene(sceneAdmin);
                });
        FlowPane root3 = new FlowPane();
        root.setPadding(new Insets(10));
        root.getChildren().add(removeP);
        Scene removePerson = new Scene(root3, 200, 400);


///////////// add items
        TextField t1 = new TextField("title");
        TextField t2 = new TextField("author");
        TextField t3 = new TextField("type of book");
        TextField t4 = new TextField("description");
        TextField t5 = new TextField("how many i want add");
        TextField t6 = new TextField("number of page or minutes to listening");
         //todo: dodać dodawanie enuma  dla typu ksiązki
        Button add3 = new Button("Add Book");
        add3.setOnAction(e ->{
            boolean message = listaItems.addItem(new Book(t1.getText(),t2.getText(),null,t4.getText(),Integer.valueOf(t5.getText()),t6.getText()));
            UtilForJavaFX.showAlertMessage(String.valueOf(message));
            window.setScene(sceneAdmin);
        });

        Button add4 = new Button("Add AudioBook");
        add4.setOnAction(e -> {
            boolean message = listaItems.addItem(new Audiobook(t1.getText(),t2.getText(),null,t4.getText(),Integer.valueOf(t5.getText()),t6.getText()));
            UtilForJavaFX.showAlertMessage(String.valueOf(message));
            window.setScene(sceneAdmin);
        });

        FlowPane root2 = new FlowPane();
        root2.setPadding(new Insets(10));
        root2.getChildren().addAll(t1,t2,t3,t4,t5,t6,add3,add4);
        Scene addItem = new Scene(root2, 200, 400);


////


          window.setScene(addItem);
          window.show();
    }



}
