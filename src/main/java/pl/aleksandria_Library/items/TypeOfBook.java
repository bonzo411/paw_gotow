package pl.aleksandria_Library.items;

public enum TypeOfBook {

    FANTASY,
    DRAMA;
}
