package pl.aleksandria_Library.items;

public class Audiobook extends Item {

    private String durationInMinutes;

    public Audiobook(String title, String author, TypeOfBook type, String description, int howManyHave, String durationInMinutes) {
        super(title, author, type, description, howManyHave);
        this.durationInMinutes = durationInMinutes;
    }

    public String getDurationInMinutes() {
        return durationInMinutes;
    }


}
