package pl.aleksandria_Library.users;

import java.util.Objects;

public abstract class Person {

  private  String name;
  private  String lastName;
  private  String emailAddress;
  private  String phoneNumber;
  private  int id;
  private   String pesel;
  private static  int COUNT = 1;

  public Person(String name, String lastName, String emailAddress, String phoneNumber, String pesel) {
    this.pesel = pesel;
    this.id = COUNT++;
    this.name = name;
    this.lastName = lastName;
    this.emailAddress = emailAddress;
    this.phoneNumber = phoneNumber;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Person person = (Person) o;
    return Objects.equals(pesel, person.pesel);
  }

  @Override
  public int hashCode() {
    return Objects.hash(pesel);
  }

  public String getPesel() {
    return pesel;
  }
}
