package pl.aleksandria_Library.users;

import org.junit.Assert;
import org.junit.Test;
import pl.aleksandria_Library.library.UsersManager;

public class UsersManagerTests {

    @Test
    public void addPerson_addOnePerson_true(){
        UsersManager lista = new UsersManager();

        final boolean actual =  lista.addPerson(new Reader("pawel","nowy","paw@pl","121212","123123123"));

        Assert.assertTrue(actual);
    }

    @Test
    public void addPerson_twoThisSamePerson_false(){
        UsersManager lista = new UsersManager();
        lista.addPerson(new Reader("pawel","nowy","paw@pl","121212","123123123"));
        final boolean actual = lista.addPerson(new Reader("pawel","nowy","paw@pl","121212","123123123"));

        Assert.assertFalse(actual);
    }

    @Test
    public void removePerson_removeOnePeresonWithEmptyList_False(){
        UsersManager lista = new UsersManager();
        final boolean actual = lista.removePerson("121212");
        Assert.assertFalse(actual);
    }

    @Test
    public void removePerson_removeOnePereson_True(){
        UsersManager lista = new UsersManager();
        lista.addPerson(new Admin("paw","nowy","awe@","1212","121212"));
        final boolean actual = lista.removePerson("121212");
        Assert.assertTrue(actual);
    }





}
